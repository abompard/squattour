==========
Squat Tour
==========

Squat Tour is a simple Django app to create a map of your friends' home address
or city. Depending on the nature of your relationship with those friends, this
map lets you do the following:

- choose where to spend your next vacation and save on housing costs ;
- when you're visiting a city, remember to say hi ;
- when you're visiting a city, remember to not post pictures of you there on
  Facebook, Instagram, etc. to avoid having to explain to your "friend" why you
  didn't stop to say hi.

Detailed documentation is in the "docs" directory.


Quick start
-----------

1. Add "squattour" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'squattour',
    ]

2. Include the squattour URLconf in your project urls.py like this::

    url(r'^squattour/', include('squattour.urls')),

3. Run `python manage.py migrate` to create the squattour models.

4. If you want to enable social login, create application authentication tokens
   with the services you need in the app's web UI, and set them in the
   ``settings.py`` file.

5. Start the development server and visit http://127.0.0.1:8000/squattour/.


Copyright and license
---------------------

Squat Tour is copyright (C) 2016  Aurelien Bompard

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
