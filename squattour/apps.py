from django.apps import AppConfig


class SquatTourConfig(AppConfig):
    name = 'squattour'
    verbose_name = "Squat Tour"

    def ready(self):
        import squattour.signals
