from django.contrib import admin

# Register your models here.

from .models import Contact, SocialContact

admin.site.register(Contact)
admin.site.register(SocialContact)
