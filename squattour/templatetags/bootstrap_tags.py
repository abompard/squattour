from django import template


register = template.Library()


@register.filter(name='add_form_control')
def add_form_control(field):
    widget_class = field.field.widget.attrs.get("class", "")
    widget_class += " form-control"
    return field.as_widget(attrs={'class': widget_class})


@register.filter('fieldtype_is')
def fieldtype_is(field, widget_class):
    return widget_class in [
        parent.__name__ for parent in
        field.field.widget.__class__.__mro__]


@register.filter('is_checkbox')
def is_checkbox(field):
    return field.field.widget.__class__.__name__ in (
        'CheckboxInput', 'CheckboxSelectMultiple')


@register.inclusion_tag('bootstrap/form.html',
                        takes_context=True)
def bootstrap_form(context, form, button=None):
    return dict(
        form=form,
        button=button,
        with_label=True,
        )


@register.inclusion_tag('bootstrap/form-horizontal.html',
                        takes_context=True)
def bootstrap_form_horizontal(
        context, form, size_left=2, size_right=8, button=None,
        fold_class='sm'):
    return dict(
        form=form,
        size_left=size_left,
        size_right=size_right,
        button=button,
        fold_class=fold_class,
        with_label=True,
        )


@register.inclusion_tag('bootstrap/form-inline.html', takes_context=True)
def bootstrap_form_inline(context, form, button=None, with_label=True):
    return dict(form=form, button=button, with_label=with_label)


@register.inclusion_tag('bootstrap/form-field.html', takes_context=True)
def bootstrap_form_field(context, form, fieldname, with_label=True):
    field = form.fields[fieldname]
    return dict(field=field, with_label=with_label)
