from django import template
from django.shortcuts import resolve_url


register = template.Library()


@register.inclusion_tag('squattour/snippets/nav_link.html',
                        takes_context=True)
def nav_link(context, view_name, label, icon=None, next=None):
    url = resolve_url(view_name)
    is_active = context["request"].path.startswith(url)
    context = dict(
        url=url,
        label=label,
        icon=icon,
        is_active=is_active,
        )
    if next:
        context["query_string"] = "next={}".format(next)
    return context
