# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('socialaccount', '0003_extra_data_default_dict'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('address', models.TextField(null=True)),
                ('latitude', models.FloatField(blank=True, max_length=100, null=True)),
                ('longitude', models.FloatField(blank=True, max_length=100, null=True)),
                ('show', models.BooleanField(default=True)),
                ('user', models.ForeignKey(related_name='squattour_contacts', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False)),
                ('user', models.OneToOneField(related_name='squattour_profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SocialContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('social_uid', models.CharField(max_length=255)),
                ('address', models.TextField(editable=False, null=True)),
                ('latitude', models.FloatField(max_length=100, editable=False, null=True)),
                ('longitude', models.FloatField(max_length=100, editable=False, null=True)),
                ('photo_url', models.CharField(blank=True, max_length=255, null=True)),
                ('contact', models.OneToOneField(related_name='social', to='squattour.Contact')),
                ('socialaccount', models.ForeignKey(related_name='squattour_contacts', to='socialaccount.SocialAccount')),
            ],
        ),
    ]
