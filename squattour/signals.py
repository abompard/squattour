import logging

from allauth.account.signals import user_logged_in
from allauth.socialaccount.models import SocialApp
from allauth.socialaccount.signals import social_account_removed
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Profile, Contact
from .lib.mapping import geocode
from .lib.contact import get_connector


logger = logging.getLogger(__name__)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_profile_post_save(sender, **kwargs):
    Profile.objects.get_or_create(user=kwargs["instance"])


@receiver(user_logged_in)
def create_profile_login(sender, **kwargs):
    Profile.objects.get_or_create(user=kwargs["user"])


@receiver(post_save, sender=Contact)
def geocode_contact(sender, **kwargs):
    contact = kwargs["instance"]
    geocode(contact)


@receiver(social_account_removed)
def on_social_account_added(sender, **kwargs):
    # Sent after a user disconnects a social account from their local account.
    social_account = kwargs["socialaccount"]
    request = kwargs["request"]
    logger.debug("Social account %s removed for user %s",
                 social_account.provider, social_account.user.username)
    social_app = SocialApp.objects.get_current(
        social_account.provider, request)
    connector = get_connector(social_account, social_app)
    connector.purge()
