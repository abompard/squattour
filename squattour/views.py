import json
import uuid

from allauth.socialaccount.models import SocialAccount, SocialApp
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied, SuspiciousOperation
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.template import loader, Context, Template

from .models import Contact, GroupContact, Profile
from .lib.mapping import get_service
from .lib.contact import get_connector


def index(request):
    context = {}
    return render(request, 'squattour/index.html', context)


@login_required
def contacts(request):
    if request.method == "POST":
        contact = get_object_or_404(Contact, pk=request.POST["contact"])
        if request.POST["action"] == "hide":
            contact.show = False
            contact.save()
        elif request.POST["action"] == "show":
            contact.show = True
            contact.save()
        elif request.POST["action"] == "reimport":
            pass # TODO
        return redirect("squattour:contacts")

    contacts = Contact.objects.filter(
        user=request.user, show=True
        ).order_by("name").select_related("social").all()
    paginator = Paginator(contacts, 50)
    page = request.GET.get('page')
    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        contacts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        contacts = paginator.page(paginator.num_pages)

    social_apps = {}
    for contact in contacts:
        if not contact.social:
            continue
        provider = contact.social.socialaccount.provider
        if provider not in social_apps:
            social_apps[provider] = SocialApp.objects.get_current(
                provider, request)
        contact.social_app = social_apps[provider]

    context = {
        "contacts": contacts,
    }
    return render(request, 'squattour/contacts.html', context)


@login_required
def import_contacts(request):
    context = {
        "accounts": SocialAccount.objects.filter(user=request.user),
        }
    return render(request, 'squattour/import.html', context)


@login_required
def show_map(request):
    service = get_service()
    context = {
        "service": service,
        "kml_url": request.build_absolute_uri(
            reverse("squattour:export", kwargs={
                    "token": request.user.squattour_profile.token.hex,
                    "fmt": "kml",
                    })
            ),
        "geojson_url": request.build_absolute_uri(
            reverse("squattour:export", kwargs={
                    "token": request.user.squattour_profile.token.hex,
                    "fmt": "json",
                    })
            ),
    }
    return render(request, service.template, context)


def export(request, token=None, fmt="kml"):
    # Export to KML or GeoJSON
    if request.user.is_authenticated:
        user = request.user
    else:
        try:
            user = Profile.objects.filter(token=uuid.UUID(hex=token)).one()
        except Profile.DoesNotExist:
            raise PermissionDenied
    all_contacts = list(Contact.objects.filter(
            user=user, show=True
        ).exclude(
            latitude=None, social__latitude=None
        ).exclude(
            longitude=None, social__longitude=None
        ).select_related("social"))

    # Merge contacts at the same address
    contacts_by_coords = {}
    for contact in all_contacts:
        if contact.coordinates not in contacts_by_coords:
            contacts_by_coords[contact.coordinates] = []
        contacts_by_coords[contact.coordinates].append(contact)
    contacts = []
    for coordinates, contact_list in contacts_by_coords.items():
        if len(contact_list) == 1:
            contacts.append(contact_list[0])
        else:
            contacts.append(GroupContact(*contact_list))


    response = HttpResponse()

    if fmt == "kml":
        # Create the HttpResponse object with the appropriate header.
        response.content_type = 'application/vnd.google-earth.kml+xml'
        t = loader.get_template('squattour/kml.xml')
        c = Context({"contacts": contacts})
        response.write(t.render(c))

    elif fmt == "json":
        popup_template = Template("""
            <div class="squattour-popup">
            <h1>{{ contact.name }}</h1>
            <p>
                {% for photo_url in contact.photo_urls %}
                    <img src="{{ photo_url }}" class="photo" />
                {% endfor %}
                {{ contact.best_address }}
            </p>
            </div>
            """)
        data = {"type": "FeatureCollection", "features": []}
        for contact in contacts:
            weight = len(contact) if isinstance(contact, GroupContact) else 1
            data["features"].append({
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        contact.best_longitude,
                        contact.best_latitude
                        ],
                    },
                "properties": {
                    "name": contact.name,
                    "popupContent": popup_template.render(Context({
                        "contact": contact})),
                    "weight": weight,
                    }
                })
        response = HttpResponse(content_type='application/javascript')
        response.write(json.dumps(data, indent=2))

    response['Content-Disposition'] = \
        'attachment; filename="friends.{}"'.format(fmt)
    return response


@login_required
def import_social(request, account):
    social_account = get_object_or_404(SocialAccount, pk=int(account))
    if social_account.user != request.user:
        raise PermissionDenied
    social_app = SocialApp.objects.get_current(
        social_account.provider, request)
    if "import" not in request.session:
        request.session["import"] = {"status": "init", "contacts": []}
    return render(request, 'squattour/import_social.html', {
        "social_account": social_account,
        "social_app": social_app,
        })


@login_required
def poll_import_status(request, account):
    social_account = get_object_or_404(SocialAccount, pk=int(account))
    if social_account.user != request.user:
        raise PermissionDenied

    if request.method == "POST":
        if request.session["import"]["status"] != "init":
            raise SuspiciousOperation
        # Start the process
        request.session["import"] = {"status": "working", "contacts": []}
        request.session.save()
        social_app = SocialApp.objects.get_current(
            social_account.provider, request)
        connector = get_connector(social_account, social_app)
        def add_to_session(contact):
            request.session["import"]["contacts"].append(contact.contact.name)
            request.session.save()
        connector.import_all(callback=add_to_session)
        messages.success(
            request,
            "%s contacts successfully imported."
            % len(request.session["import"]["contacts"]))
        del request.session["import"]
        return HttpResponse(json.dumps({"status": "done"}),
                            content_type='application/javascript')

    else:
        if "import" in request.session:
            return HttpResponse(json.dumps(request.session["import"]),
                                content_type='application/javascript')
        else:
            return HttpResponse(json.dumps({"status": "done"}),
                                content_type='application/javascript')

