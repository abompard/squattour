from django.conf.urls import include, url

from . import views

app_name = 'squattour'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^map/$', views.show_map, name='map'),
    url(r'^export/friends.(?P<fmt>[\w]+)$', views.export, name='export'),
    url(r'^export/(?P<token>[\w]{32})/friends.(?P<fmt>[\w]+)$',
        views.export, name='export'),
    url(r'^contacts/import/$', views.import_contacts, name='import'),
    url(r'^contacts/import/social/$',
        views.import_social, name='import_social'),
    url(r'^contacts/import/social/(?P<account>[0-9]+)/$',
        views.import_social, name='import_social'),
    url(r'^contacts/import/social/(?P<account>[0-9]+)/status$',
        views.poll_import_status, name='poll_import_status'),
]
