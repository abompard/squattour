import logging
import requests

from allauth.socialaccount.models import SocialToken
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from datetime import timedelta
from django.conf import settings
from django.utils import timezone
from squattour.models import Contact, SocialContact

from .mapping import geocode


log = logging.getLogger(__name__)


class ContactConnector:

    def __init__(self, social_account, social_app):
        self.social_account = social_account
        self.social_app = social_app

    def import_all(self, callback=None):
        pass

    def import_one(self):
        pass

    def purge(self):
        Contact.objects.filter(
            user=self.socia_account.user,
            social__socialaccount=self.social_account).delete()


class GoogleContacts(ContactConnector):

    adapter = GoogleOAuth2Adapter

    def import_all(self, callback=None):
        token = self._get_token()
        url = "https://people.googleapis.com/v1/people/me/connections"
        query_string = {
            "access_token": token,
            "sortOrder": "FIRST_NAME_ASCENDING",
            "requestMask.includeField": ",".join([
                    "person.{}".format(field) for field in
                    ("names", "nicknames", "photos", "addresses", "residences")
                ]),
        }
        has_more_contacts = True
        while has_more_contacts:
            response = requests.get(url, query_string)
            result = response.json()
            if "nextPageToken" in result:
                query_string["pageToken"] = result["nextPageToken"]
            else:
                has_more_contacts = False
            for contact in result["connections"]:
                social_contact = self._import_contact(contact)
                if callback is not None and social_contact is not None:
                    callback(social_contact)

    def _get_token(self):
        try:
            token = SocialToken.objects.get(
                app=self.social_app,
                account=self.social_account)
        except SocialToken.DoesNotExist:
            raise RuntimeError(
                "Can't find a token for %s"
                % self.social_account.user.username)
        if token.expires_at <= timezone.now():
            log.info("Refreshing token for %s / %s",
                     self.social_account.provider,
                     self.social_account.user.username)
            self._refresh_token(token)
        return token

    def _refresh_token(self, token):
        data = {
            "refresh_token": token.token_secret,
            "client_id": token.app.client_id,
            "client_secret": token.app.secret,
            "grant_type": "refresh_token",
        }
        response = requests.post(self.adapter.access_token_url, data=data)
        response = response.json()
        token.token = response["access_token"]
        expires_in = response.get(self.adapter.expires_in_key, None)
        if expires_in:
            token.expires_at = timezone.now() + timedelta(
                seconds=int(expires_in))
        token.save()

    def _import_contact(self, data):
        if "addresses" not in data and "residences" not in data:
            return # no location data, skip it
        uid = data["resourceName"]
        name = self._find_primary(data["names"])["displayName"]
        log.debug("Importing %s: %s", uid, name)
        try:
            social_contact = SocialContact.objects.get(
                socialaccount=self.social_account, social_uid=uid
                ).select_related("contact")
        except SocialContact.DoesNotExist:
            contact = Contact.objects.create(
                user=self.social_account.user,
                name=name)
            social_contact = SocialContact.objects.create(
                contact=contact,
                socialaccount=self.social_account,
                social_uid=uid)
            social_contact.import_status = "added"
        else:
            contact = social_contact.contact
            if contact.name != name:
                contact.name = name
                contact.save()
            social_contact.import_status = "updated"
        # Update socialcontact data
        if "photos" in data:
            social_contact.photo_url = self._find_primary(data["photos"])["url"]
        self._set_address(social_contact, data)
        social_contact.save()
        return social_contact

    def _find_primary(self, data):
        for value in data:
            if value["metadata"].get("primary"):
                return value

    def _set_address(self, social_contact, data):
        old_address = social_contact.address
        if "addresses" in data:
            social_contact.address = self._find_primary(data["addresses"])["formattedValue"]
        elif "residences" in data:
            social_contact.address = self._find_primary(data["residences"])["value"]
        if social_contact.address != old_address:
            social_contact.latitude = None
            social_contact.longitude = None
        geocode(social_contact)


class FacebookContacts(ContactConnector):
    pass


CONNECTORS = {
    "google": GoogleContacts,
    "facebook": FacebookContacts,
}

def get_connector(social_account, social_app):
    CONNECTORS.update(getattr(
        settings, "SQUATTOUR_CONTACTS_CONNECTORS", {}))
    connector = CONNECTORS[social_account.provider](social_account, social_app)
    return connector
