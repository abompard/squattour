import json
import logging
import requests

from django.conf import settings
from importlib import import_module
from urllib.parse import urljoin

log = logging.getLogger(__name__)


class MappingService:

    name = None

    @property
    def template(self):
        return "squattour/mapping/{}.html".format(self.name)

    def geocode(self, address):
        return NotImplementedError


class OpenStreetMap(MappingService):

    name = "openstreetmap"

    def geocode(self, address):
        params = {
            "q": address,
            "format": "json",
            "limit": "1",
            "addressdetails": "0",
        }
        log.info("Geocoding %s from OpenStreetMap", address)
        response = requests.get(
            "http://nominatim.openstreetmap.org/search", params)
        result = response.json()
        if not result:
            return None
        return {"longitude": result[0]["lon"],
                "latitude": result[0]["lat"],
                }


class GoogleMaps(MappingService):

    name = "gmaps"

    @property
    def client_key(self):
        return settings.SQUATTOUR_GOOGLE_CLIENT_KEY

    @property
    def server_key(self):
        return settings.SQUATTOUR_GOOGLE_SERVER_KEY

    def geocode(self, address):
        params = {
            "address": address,
            "key": self.server_key,
        }
        log.info("Geocoding %s from Google Maps", address)
        response = requests.get(
            "https://maps.googleapis.com/maps/api/geocode/json", params)
        result = response.json()
        if result["status"] != "OK" or not result["results"]:
            return None
        location = result["results"][0]["geometry"]["location"]
        return {"longitude": location["lng"],
                "latitude": location["lat"],
                }


def get_service():
    classpath = getattr(
        settings, "SQUATTOUR_MAPPING_SERVICE",
        "squattour.lib.mapping.OpenStreetMap")
    module, _sep, classname = classpath.rpartition(".")
    module = import_module(module)
    return getattr(module, classname)()


def geocode(contact):
    if contact.latitude and contact.longitude:
        return  # Already set
    if not contact.address:
        return  # Nothing we can do
    service = get_service()
    result = service.geocode(contact.address)
    if not result:
        return  # Geocoding failed
    contact.latitude = result["latitude"]
    contact.longitude = result["longitude"]
    contact.save()
