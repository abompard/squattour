import uuid

from allauth.socialaccount.models import SocialAccount
from django.conf import settings
from django.db import models


class Contact(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="squattour_contacts",
        on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    address = models.TextField(null=True)
    latitude = models.FloatField(max_length=100, null=True, blank=True)
    longitude = models.FloatField(max_length=100, null=True, blank=True)
    show = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def best_address(self):
        return self.address or self.social.address

    @property
    def best_latitude(self):
        return self.latitude or self.social.latitude

    @property
    def best_longitude(self):
        return self.longitude or self.social.longitude

    @property
    def coordinates(self):
        return "{},{}".format(self.best_longitude, self.best_latitude)

    @property
    def photo_urls(self):
        if self.social.photo_url:
            return [self.social.photo_url]
        else:
            return []


class GroupContact:

    def __init__(self, *args):
        self._contacts = args
        for contact in self._contacts:
            if not isinstance(contact, Contact):
                raise ValueError(
                    "A GroupContact can only contain Contact objects")
        self.name = ", ".join([c.name for c in self._contacts])
        for attribute in ("best_latitude", "best_longitude"):
            if len(set([getattr(c, attribute) for c in self._contacts])) != 1:
                raise ValueError(
                    "All Contacts must have the same %s" % attribute)
            setattr(self, attribute, getattr(self._contacts[0], attribute))
        self.coordinates = "{},{}".format(
            self.best_longitude, self.best_latitude)
        # It may be the same address but spelled differently.
        self.best_address = self._contacts[0].best_address

    def __len__(self):
        return len(self._contacts)

    @property
    def photo_urls(self):
        photos = []
        for contact in self._contacts:
            if contact.social.photo_url:
                photos.append(contact.social.photo_url)
        return photos


class SocialContact(models.Model):
    contact = models.OneToOneField(
        Contact, related_name="social", on_delete=models.CASCADE)
    socialaccount = models.ForeignKey(
        SocialAccount, related_name="squattour_contacts",
        on_delete=models.CASCADE)
    social_uid = models.CharField(max_length=255)
    address = models.TextField(null=True, editable=False)
    latitude = models.FloatField(max_length=100, null=True, editable=False)
    longitude = models.FloatField(max_length=100, null=True, editable=False)
    photo_url = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "{} ({})".format(
            self.contact.name, self.socialaccount.provider)


class Profile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name="squattour_profile",
        on_delete=models.CASCADE)
    token = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
